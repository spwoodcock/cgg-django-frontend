import logging

from .base import *

# Check production settings
# See https://docs.djangoproject.com/en/3.0/howto/deployment/checklist/


SECRET_KEY = os.getenv("SECRET_KEY")

DEBUG = False

ALLOWED_HOSTS = [os.getenv("HOST_URL")]

# Prefer X-Forwarded-Host header over Host - from Traefik proxy
USE_X_FORWARDED_HOST = True
# SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# URL Prefix Setting Required
FORCE_SCRIPT_NAME = "/frontend"

# Set both true in HTTPS environment
# Avoid sending CSRF or session cookies over HTTP
# CSRF_COOKIE_SECURE = True
# SESSION_COOKIE_SECURE = True


# # Cache Config (e.g. Redis server IP, local DB table)
# CACHES = {
#    'default': {
#       'BACKEND': 'django.core.cache.backends.db.DatabaseCache',
#       'LOCATION': 'django_cache',
#    }
# }


# Django Logging Config
log_dir = os.getenv("DEFAULT_LOG_DIR", "/<redacted>/logs/")
log_level = os.getenv("DJANGO_LOG_LEVEL", "DEBUG")
LOGGING = {
    "version": 1,
    "disable_existing_loggers": False,
    "formatters": {
        "cgg_standard": {
            "format": (
                "[%(asctime)s.%(msecs)03d]  %(levelname)s  |  %(name)s  |  %(message)s"
            ),
            "datefmt": "%Y-%m-%d %H:%M:%S",
        },
    },
    "handlers": {
        "frontendlogfile": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": log_dir + "/frontend/frontend.log",
            "maxBytes": 1024 * 1024 * 15,  # 15MB
            "backupCount": 10,
            "formatter": "cgg_standard",
        },
        "airpollogfile": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": log_dir + "/frontend/airpol.log",
            "maxBytes": 1024 * 1024 * 15,  # 15MB
            "backupCount": 10,
            "formatter": "cgg_standard",
        },
        "sandmanlogfile": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": log_dir + "/frontend/sandman.log",
            "maxBytes": 1024 * 1024 * 15,  # 15MB
            "backupCount": 10,
            "formatter": "cgg_standard",
        },
        "whamlogfile": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": log_dir + "/frontend/wham.log",
            "maxBytes": 1024 * 1024 * 15,  # 15MB
            "backupCount": 10,
            "formatter": "cgg_standard",
        },
        "simpllogfile": {
            "level": "DEBUG",
            "class": "logging.handlers.RotatingFileHandler",
            "filename": log_dir + "/frontend/simpl.log",
            "maxBytes": 1024 * 1024 * 15,  # 15MB
            "backupCount": 10,
            "formatter": "cgg_standard",
        },
        "console": {
            "level": "DEBUG",
            "class": "logging.StreamHandler",
            "formatter": "cgg_standard",
        },
    },
    "loggers": {
        "": {
            "handlers": ["frontendlogfile", "console"],
            "level": os.getenv("ROOT_LOG_LEVEL", "ERROR"),
        },
        "airpol": {
            "handlers": ["airpollogfile"],
            "level": log_level,
        },
        "sandman": {
            "handlers": ["sandmanlogfile"],
            "level": log_level,
        },
        "wham": {
            "handlers": ["whamlogfile"],
            "level": log_level,
        },
        "simpl": {
            "handlers": ["simpllogfile"],
            "level": log_level,
        },
    },
}
# Catch and log unhandled exceptions
sys.excepthook = lambda exc_type, exc_value, exc_traceback: logging.getLogger(
    "*excepthook*"
).critical("Uncaught Exception!", exc_info=(exc_type, exc_value, exc_traceback))


# # Whitenoise static file server (alternative to nginx)
# MIDDLEWARE.insert(1, 'whitenoise.middleware.WhiteNoiseMiddleware')
# STATICFILES_STORAGE = 'whitenoise.storage.CompressedManifestStaticFilesStorage'


# Database
# https://docs.djangoproject.com/en/3.0/ref/settings/#databases
DATABASE_ROUTERS = ["frontend.routers.DjangoMetaRouter", "frontend.routers.DBRouter"]
DATABASES = {
    "default": {
        "ENGINE": "django.db.backends.postgresql",
        "NAME": "dbdjango",
        "USER": os.getenv("DB_AIRPOL_USER"),
        "PASSWORD": os.getenv("DB_AIRPOL_PASS"),
        "HOST": os.getenv("DB_AIRPOL_HOST"),
        "PORT": os.getenv("DB_AIRPOL_PORT"),
    },
    "dbairpol": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "NAME": "dbairpol",
        "USER": os.getenv("DB_AIRPOL_USER"),
        "PASSWORD": os.getenv("DB_AIRPOL_PASS"),
        "HOST": os.getenv("DB_AIRPOL_HOST"),
        "PORT": os.getenv("DB_AIRPOL_PORT"),
    },
    "dbsandman": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "NAME": "dbsandman",
        "USER": os.getenv("DB_SANDMAN_USER"),
        "PASSWORD": os.getenv("DB_SANDMAN_PASS"),
        "HOST": os.getenv("DB_SANDMAN_HOST"),
        "PORT": os.getenv("DB_SANDMAN_PORT"),
    },
    "dbwham02": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "NAME": "dbwham02",
        "USER": os.getenv("DB_WHAM_USER"),
        "PASSWORD": os.getenv("DB_WHAM_PASS"),
        "HOST": os.getenv("DB_WHAM_HOST"),
        "PORT": os.getenv("DB_WHAM_PORT"),
        "OPTIONS": {"options": "-c search_path=public"},
    },
    "dbsimpl": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "NAME": "dbsimpl",
        "USER": os.getenv("DB_SIMPL_USER"),
        "PASSWORD": os.getenv("DB_SIMPL_PASS"),
        "HOST": os.getenv("DB_SIMPL_HOST"),
        "PORT": os.getenv("DB_SIMPL_PORT"),
    },
    "dboffshore": {
        "ENGINE": "django.contrib.gis.db.backends.postgis",
        "NAME": "db_offshore",
        "USER": os.getenv("DB_OFFSHORE_USER"),
        "PASSWORD": os.getenv("DB_OFFSHORE_PASS"),
        "HOST": os.getenv("DB_OFFSHORE_HOST"),
        "PORT": os.getenv("DB_OFFSHORE_PORT"),
    },
}
# Set POSTGIS version manually, as dbadminsw does not have permission to query
POSTGIS_VERSION = (2, 5, 1)
