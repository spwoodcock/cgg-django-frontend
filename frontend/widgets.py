from django import forms


class DateRangeWidget(forms.TextInput):
    class Media:
        css = {
            "all": ("https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css",)
        }
        js = ("https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js",
