#!/bin/bash
set -euo pipefail

if [ -v DEV_MODE ]; then

    until PGPASSWORD=$DB_AIRPOL_PASS psql -U $DB_AIRPOL_USER -h $DB_AIRPOL_HOST -d dbairpol -c '\l'; do
        >&2 echo "Postgres is unavailable - sleeping"
        sleep 1
    done

    >&2 echo "Postgres is up - continuing"

fi

python manage.py collectstatic --noinput

exec "$@"
